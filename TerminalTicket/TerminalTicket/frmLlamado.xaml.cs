﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using TerminalTicket.Controllers;
using MessageBox = System.Windows.Forms.MessageBox;

namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para frmLlamado.xaml
    /// </summary>
    public partial class frmLlamado : Window
    {

        public frmLlamado()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                MainForm mainForm = (MainForm)Owner;
                this.lblTicket.Content = mainForm.boxTicket.Text;
                this.lblVentana.Content = mainForm.boxVentana.Text;


                //int cont = 0;
                //foreach (var pantalla in Screen.AllScreens)
                //{
                //    cont = cont + 1;
                //    if (!pantalla.Primary && pantalla == Screen.AllScreens[1]) //Screen.AllScreens.Count())
                //    {

                //        //MessageBox.Show(Screen.AllScreens[1].DeviceName);

                //        WindowStartupLocation = WindowStartupLocation.Manual;
                //        Debug.Assert(SystemInformation.MonitorCount > 1);
                //        Left = (Screen.AllScreens[1].WorkingArea.Width - Screen.AllScreens[1].WorkingArea.Width) / 2;
                //        Top = (Screen.AllScreens[1].WorkingArea.Height - Screen.AllScreens[1].WorkingArea.Height) / 2;
                //        Width = Screen.AllScreens[1].WorkingArea.Width;
                //        Height = Screen.AllScreens[1].WorkingArea.Height;
                //        WindowStartupLocation = WindowStartupLocation.Manual;
                //    }
                //}
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
