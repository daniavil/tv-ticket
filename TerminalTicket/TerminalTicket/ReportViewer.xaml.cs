﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Text;
using System.Windows.Controls;
using System.IO;
using System.Windows;
using Microsoft.Reporting.WinForms;
using Path = System.IO.Path;
using TerminalTicket.Models;
using TerminalTicket.Controllers;
using System.Configuration;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Drawing;
using System.Data;
using System.Security.Permissions;

namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para ReportViewer.xaml
    /// </summary>
    public partial class ReportViewer : UserControl
    {
        public ReportViewer()
        {
            InitializeComponent();
        }

        private void ReportViewer_RenderingComplete(object sender, Microsoft.Reporting.WinForms.RenderingCompleteEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Run();
        }

        private int m_currentPageIndex;
        private IList<Stream> m_streams;

        //private Stream CreateStream(string name,string fileNameExtension, Encoding encoding,string mimeType, bool willSeek)
        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            //name = "repoImagen";

            //string archivo = $"{name}.{fileNameExtension}";
            //string exePath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            //var dataPath = exePath.Replace("TerminalTicket.exe", "");

            //if (!Directory.Exists(dataPath))
            //{
            //    Directory.CreateDirectory(dataPath);
            //}

            try
            {
                //Stream stream = null;
                //string appFile = Path.Combine(dataPath, archivo);//Se brindan permisos full sobre la carpeta

                //if (File.Exists(appFile))
                //{
                //    File.Delete(appFile);
                //}
                Stream stream = new MemoryStream(); //new FileStream(appFile, FileMode.Create);
                //File.SetAttributes(appFile, (new FileInfo(appFile)).Attributes | FileAttributes.Normal);                
                m_streams.Add(stream);
                return stream;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private bool Export(LocalReport report)
        {
            try
            {
                string deviceInfo =
              "<DeviceInfo>" +
              "  <OutputFormat>EMF</OutputFormat>" +
              "  <PageWidth>8.5in</PageWidth>" +
              "  <PageHeight>11in</PageHeight>" +
              "  <MarginTop>0in</MarginTop>" +
              "  <MarginLeft>0.25in</MarginLeft>" +
              "  <MarginRight>0.25in</MarginRight>" +
              "  <MarginBottom>0in</MarginBottom>" +
              "</DeviceInfo>";
                Warning[] warnings;

                m_streams = new List<Stream>();

                report.Render("Image", deviceInfo, CreateStream, out warnings);

                foreach (Stream stream in m_streams)
                {
                    stream.Position = 0;
                }
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("No se pudo crear el reporte para imprimir.");
                return false;
            }                         
        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new
               Metafile(m_streams[m_currentPageIndex]);
            ev.Graphics.DrawImage(pageImage, ev.PageBounds);
            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private void Print()
        {
            PrinterSettings printerName = new PrinterSettings();

            string defaultPrinter;

            defaultPrinter = printerName.PrinterName;


            if (m_streams == null || m_streams.Count == 0)
                return;
            PrintDocument printDoc = new PrintDocument();
            printDoc.PrinterSettings.PrinterName = defaultPrinter;
            if (!printDoc.PrinterSettings.IsValid)
            {
                string msg = String.Format(
                   "Can't find printer \"{0}\".", printerName);
                MessageBox.Show(msg, "Print Error");
                return;
            }
            printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
            printDoc.DefaultPageSettings.Landscape = false;
            printDoc.Print();
        }

        private void Run()
        {
            string idSucursal = ConfigurationManager.AppSettings["IdSucursal"].ToString();

            string exePath = System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName;
            string reportPath = exePath.Replace("TerminalTicket.exe", "rptTicketV2.rdlc");
            try
            {
                ObjClass ticket = DataTicket.GetUltimoTicketBySucursal(Convert.ToInt32(idSucursal));

                dsGen ds = new dsGen();
                DataRow dr = ds.Tables[0].NewRow();
                dr[0] = ticket.Texto;
                dr[1] = ticket.Id;
                dr[2] = ticket.Texto1;
                ds.Tables[0].Rows.Add(dr);
                                
                reportViewer.ProcessingMode = ProcessingMode.Local;
                reportViewer.LocalReport.ReportPath = reportPath;
                
                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "dsGen";
                reportDataSource.Value = ds.Tables[0];
                reportViewer.LocalReport.DataSources.Add(reportDataSource);
                reportViewer.RefreshReport();

                if (Export(reportViewer.LocalReport))
                {
                    m_currentPageIndex = 0;
                    Print();
                    Dispose();
                }                
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
                MessageBox.Show(reportPath + Environment.NewLine + ex.ToString());
            }
        }

        public void Dispose()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                    stream.Close();
                m_streams = null;
            }
        }
    }
}
