﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using TerminalTicket.Controllers;
using TerminalTicket.DTOs;

namespace TerminalTicket
{
	/// <summary>
	/// Interaction logic for ModalDialog.xaml
	/// </summary>
	public partial class ModalDialogAteEmp : UserControl
	{
        private string _stringTokent="";
		public ModalDialogAteEmp()
		{
			InitializeComponent();
			Visibility = Visibility.Hidden;
        }

		private bool _hideRequest = false;
        private RespGenerica _result = new RespGenerica();
		private UIElement _parent;

		public void SetParent(UIElement parent)
		{
            _parent = parent;
        }
		public RespGenerica ShowHandlerDialog()
		{
			Visibility = Visibility.Visible;

            /*****************************************************************************/
            txtCodToken.Focus();
            txtCodToken.Text = string.Empty;
            _stringTokent = txtCodToken.Text;
            txtCodToken.IsReadOnly = true;
            /*****************************************************************************/

            _hideRequest = false;
            while (!_hideRequest)
            {
                if (this.Dispatcher.HasShutdownStarted ||this.Dispatcher.HasShutdownFinished)
                {
                    break;
                }
                this.Dispatcher.Invoke(DispatcherPriority.Background,new ThreadStart(delegate { }));
            }

            return _result;
		}
		
		private void HideHandlerDialog()
		{
            Thread.Sleep(500);
            _hideRequest = true;
			Visibility = Visibility.Hidden;
            _parent.IsEnabled = true;
		}

        private void BtnCancel_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            _result.esValido = false;
            _result.msjResp = string.Empty;
            HideHandlerDialog();
        }

        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            /*************************************/
            _result = DataTicket.VerificarClienteEmpresarial(txtCodToken.Text.Trim());
            /*************************************/
            HideHandlerDialog();
        }

        private void Btn1_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "1";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn2_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "2";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn3_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "3";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn4_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "4";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn5_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "5";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn6_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "6";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn7_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "7";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn8_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "8";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn9_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "9";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void Btn0_Click(object sender, RoutedEventArgs e)
        {
            _stringTokent = _stringTokent + "0";
            txtCodToken.Text = _stringTokent;
            txtCodToken.Focus();
        }

        private void BtnDel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _stringTokent = _stringTokent.Remove(_stringTokent.Length - 1);
                txtCodToken.Text = _stringTokent;
            }
            catch (Exception)
            {
                txtCodToken.Text = string.Empty;
                txtCodToken.Focus();
            }

            
        }

        
    }
}
