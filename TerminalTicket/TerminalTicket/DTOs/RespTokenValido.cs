﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalTicket.DTOs
{
    public class RespGenerica
    {
        public bool esValido { get; set; }
        public string msjResp { get; set; }
    }
}
