﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalTicket.DTOs
{
    public class ClienteDto
    {
        public int Nis_rad { get; set; }
        public int Clave { get; set; }
        public string NombreCliente { get; set; }
        public string Codmercado { get; set; }
        public string Mercado { get; set; }
        public bool EsEmp { get; set; }
    }
}
