﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using TerminalTicket.Controllers;
using Button = System.Windows.Controls.Button;
using Control = System.Windows.Controls.Control;
using Cursors = System.Windows.Input.Cursors;
using MessageBox = System.Windows.Forms.MessageBox;

namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para FrmMenuTicket.xaml
    /// </summary>
    public partial class FrmMenuTicket : Window
    {
        private string Version = "0";
        public FrmMenuTicket()
        {
            InitializeComponent();
            //this.DataContext = this;

            Version = "v" + ConfigClass.idVersion;

            txtVersion.Text = Version;

            if (Screen.AllScreens.Length <= 1)
            {
                if (MessageBox.Show(
                        "ATENCIÓN.!SOLO TIENE UNA PANTALLA DISPONIBLE, POR FAVOR CONECTE EL SEGUNDO MONITOR PARA EJECUTAR ESTE PROGRAMA.",
                        "INFORMACIÓN", MessageBoxButtons.OK) == System.Windows.Forms.DialogResult.OK)
                {
                    Process.GetCurrentProcess().Kill();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private void RaisePropertyChanged(string propName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        private int myCounter = 0;
        
        public int MyCounter
        {
            get { return myCounter; }
            protected set
            {
                if (value != myCounter)
                {
                    myCounter = value;
                    RaisePropertyChanged("MyCounter");
                }
            }
        }
        private void RepeatButton_Click(object sender, RoutedEventArgs e)
        {
            MyCounter++;

            if (MyCounter == 20)
            {
                DialogResult dialogResult = MessageBox.Show("¿Desea Salir del Programa?", "SALIR DE TV TURNO EEH", MessageBoxButtons.YesNo);
                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
                else
                {
                    MyCounter = 0;
                }
            }
        }

        private void FrmMenuTicket_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {

                    foreach (var ctrl in GridPrincipal.Children)
                    {
                        if (ctrl.GetType() == typeof(Button))
                        {
                            Button button = new Button();
                            button = (Button)ctrl;
                            if (DataMenu.MuestraOcultaBotonMenu(button.Name))
                            {
                                button.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                button.Visibility = Visibility.Hidden;
                            }
                        }
                    }

                    Mouse.OverrideCursor = Cursors.None;
                    foreach (var pantalla in Screen.AllScreens)
                    {
                        if (pantalla.Primary)
                        {
                            var workingArea = pantalla;
                            var area = workingArea.WorkingArea;
                            this.Left = area.Left;
                            this.Top = area.Top;
                            this.Width = area.Width;
                            this.Height = area.Height;
                            this.WindowStartupLocation = WindowStartupLocation.Manual;
                            this.WindowState = WindowState.Maximized;
                        }
                    }

                    MainForm FormVideo = new MainForm();
                    Screen s1 = Screen.AllScreens[1];
                    System.Drawing.Rectangle r1 = s1.WorkingArea;
                    FormVideo.WindowState = WindowState.Normal;
                    FormVideo.WindowStartupLocation = WindowStartupLocation.Manual;
                    FormVideo.Top = r1.Top;
                    FormVideo.Left = r1.Left;
                    FormVideo.Show();
                }
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu1_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu2_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu2_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu3_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu3_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu4_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu4_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                string content = (sender as Button).Name;
                if (DataTicket.CrearTicket(content))
                {
                    TicketReport rpt = new TicketReport();
                    rpt.Show();
                    rpt.Hide();
                }
                FrmTicket frmTicket = new FrmTicket();
                frmTicket.ShowDialog();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void menu5_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            /*
            try
            {
                ModalDialog.SetParent(this);
                var res = ModalDialog.ShowHandlerDialog();

                if (!string.IsNullOrEmpty(res.msjResp))
                {
                    if (res.esValido)
                    {
                        string content = (sender as Button).Name;
                        if (DataTicket.CrearTicket(content))
                        {
                            TicketReport rpt = new TicketReport();
                            rpt.Show();
                            rpt.Hide();
                        }
                        FrmTicket frmTicket = new FrmTicket();
                        frmTicket.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show(res.msjResp);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            */
        }

        private void menu5_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                ModalDialog.SetParent(this);
                var res = ModalDialog.ShowHandlerDialog();

                if (!string.IsNullOrEmpty(res.msjResp))
                {
                    if (res.esValido)
                    {
                        string content = (sender as Button).Name;
                        if (DataTicket.CrearTicket(content))
                        {
                            TicketReport rpt = new TicketReport();
                            rpt.Show();
                            rpt.Hide();
                        }
                        FrmTicket frmTicket = new FrmTicket();
                        frmTicket.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show(res.msjResp);
                    }
                }
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Menu6_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            try
            {
                ModalDialogAteEmp.SetParent(this);
                var res = ModalDialogAteEmp.ShowHandlerDialog();

                if (!string.IsNullOrEmpty(res.msjResp))
                {
                    if (res.esValido)
                    {
                        string content = (sender as Button).Name;
                        if (DataTicket.CrearTicket(content))
                        {
                            TicketReport rpt = new TicketReport();
                            rpt.Show();
                            rpt.Hide();
                        }
                        FrmTicket frmTicket = new FrmTicket();
                        frmTicket.ShowDialog();
                    }
                    else
                    {
                        MessageBoxError frm = new MessageBoxError(res.msjResp);
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Menu6_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
        }

        /***********************************************************************************************/
        private void frmMenuTicket_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.F2)
            {
                DialogResult dialogResult = MessageBox.Show("¿Desea Salir del Programa?", "SALIR DE TV TURNO EEH", MessageBoxButtons.YesNo);
                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
            }
        }

        private void FrmMenuTicket_Closed(object sender, EventArgs e)
        {
            //Fun.ReportError("Error Método FrmMenuTicket_Closed",
            //        MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
            //        MethodBase.GetCurrentMethod().Name);
        }

        private void FrmMenuTicket_Closing(object sender, CancelEventArgs e)
        {
            //Fun.ReportError("Evento Cerrando - FrmMenuTicket_Closing",
            //        MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
            //        MethodBase.GetCurrentMethod().Name);
        }
    }
}
