﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Speech.Synthesis;
using System.Timers;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;
using TerminalTicket.Controllers;
using TerminalTicket.Models;
using Cursors = System.Windows.Input.Cursors;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.Forms.MessageBox;
using Timer = System.Timers.Timer;


namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para MainForm.xaml
    /// </summary>
    public partial class MainForm : Window
    {
        //public static Timer TimerLoad;
        private static Timer _timerCliente;
        private static Timer _timerGridCliente;
        private static Timer _timerMsj;
        private Timer _timerDingDong;
        private static int _indexMsjInicial = 0;
        private static int _listaCountAtcMensajes;
        private static string _atcMensaje;
        UcMarqueeText _ucTxtmsj = new UcMarqueeText();
        private readonly int _tiempoMensaje = 10;
        SpeechSynthesizer _speechSynthesizerObj;
        readonly List<VoiceInfo> _vocesInfo = new List<VoiceInfo>();
        private frmLlamado _frmLlamado;
        private string _nombre = "";
        private string _mensaje= "";
        private TICKET_Ticket _ticket;

        public MainForm()
        {
            InitializeComponent();
            Mouse.OverrideCursor = Cursors.None;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                _ucTxtmsj = this.txtMsj;
                _ucTxtmsj.MarqueeTimeInSeconds = _tiempoMensaje;
                _ucTxtmsj.MarqueeContent = CrearMensaje();
                txtSucursal.Text = DataSucursal.GetNombreSucursal();
                //EsperaLoad();
                IniciaPantallaVideo();

                Screen s1 = Screen.AllScreens[1];
                System.Drawing.Rectangle r1 = s1.WorkingArea;
                WindowState = WindowState.Normal;
                WindowStartupLocation = WindowStartupLocation.Manual;
                Top = r1.Top;
                Left = r1.Left;
                WindowState = WindowState.Maximized;
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }
        private void IniciaPantallaVideo()
        {

            try
            {
                ///reiniciar codigos de atencion
                ///y pasar tickets a historico
                if (DataTicket.GetCountClientesAtencionNotToday() > 0)
                {
                    DataTicket.ReiniciarCodAtencionBySucursal();
                    DataTicket.MigrarClientesAtencionHistorico();
                }
                MaximizeToSecondaryMonitor();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void MaximizeToSecondaryMonitor()
        {
            try
            {
                if (Screen.AllScreens.Length <= 1)
                {
                    if (MessageBox.Show("ATENCIÓN.!SOLO TIENE UNA PANTALLA DISPONIBLE, POR FAVOR CONECTE EL SEGUNDO MONITOR PARA EJECUTAR ESTE PROGRAMA.", "INFORMACIÓN", MessageBoxButtons.OK) == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else
                {
                    _listaCountAtcMensajes = DataMensaje.CountMensajes();
                    LoadVideo();
                    _timerMsj = new Timer { Interval = _tiempoMensaje * 1000 };
                    _timerMsj.Elapsed += OnTimedEventMensaje;
                    _timerMsj.AutoReset = true;
                    _timerMsj.Enabled = true;
                    _timerMsj.Start();
                    _timerCliente = new Timer { Interval = 8000 };
                    _timerCliente.Elapsed += OnTimedEvent_TimerCliente;
                    _timerCliente.AutoReset = true;
                    _timerCliente.Enabled = true;

                    _timerGridCliente = new Timer { Interval = 1000 };
                    _timerGridCliente.Elapsed += OnTimedEvent_TimerGridCliente;
                    _timerGridCliente.AutoReset = true;
                    _timerGridCliente.Enabled = true;

                    CargarClientes();
                    DispatcherTimer timer = new DispatcherTimer();
                    timer.Interval = TimeSpan.FromSeconds(1);
                    timer.Tick += timer_Tick_FechaHora;
                    timer.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    "ATENCIÓN.! Surgio Un Problema: " + ex.Message,
                    "ERROR", MessageBoxButtons.OK);
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void timer_Tick_FechaHora(object sender, EventArgs e)
        {
            try
            {
                txtFechaHora.Text = DateTime.Now.ToLongDateString() + "  /  " + DateTime.Now.ToString("h:mm:ss tt");
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void OnTimedEvent_TimerGridCliente(object source, ElapsedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal,
                (Action)(CargarClientes));
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void LoadVideo()
        {
            try
            {
                var video = ConfigurationManager.AppSettings["NombreVideo"];
                string x = (Assembly.GetEntryAssembly().Location + "");
                x = x.Replace("TerminalTicket.exe", video);
                ElementVideo.Source = new Uri(x);
                ElementVideo.Play();
                ElementVideo.Volume = 0;
                ElementVideo.MediaEnded += ElementVideo_MediaEnded;
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ElementVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            try
            {
                ElementVideo.Position = TimeSpan.FromSeconds(0);
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ElementVideo_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ElementVideo.Play();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void OnTimedEventMensaje(object source, ElapsedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal,
                (Action)(() =>
                {
                    _ucTxtmsj = this.txtMsj;
                    _ucTxtmsj.MarqueeTimeInSeconds = _tiempoMensaje;
                    _ucTxtmsj.MarqueeContent = CrearMensaje();
                }));
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private static string CrearMensaje()
        {
            try
            {
                if (_indexMsjInicial == 0)
                {
                    _atcMensaje = DataMensaje.MensajebyIndex(_indexMsjInicial);
                    _indexMsjInicial = _indexMsjInicial + 1;
                }
                else if (_indexMsjInicial < _listaCountAtcMensajes)
                {
                    _atcMensaje = DataMensaje.MensajebyIndex(_indexMsjInicial);
                    _indexMsjInicial = _indexMsjInicial + 1;
                }
                else if (_indexMsjInicial == _listaCountAtcMensajes)
                {
                    _atcMensaje = DataMensaje.MensajebyIndex(_indexMsjInicial - 1);
                    _indexMsjInicial = 0;
                }
                return _atcMensaje;
            }
            catch (Exception ex)
            {
                
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
            return null;
        }

        private void OnTimedEvent_TimerCliente(object source, ElapsedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal,
                (Action)(() =>
                {
                    _timerCliente.Stop();
                    LlamarCliente();
                }));
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void CargarClientes()
        {
            try
            {
                dgClientes.ItemsSource = DataCliente.GetListaClientesDto();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void LlamarCliente()
        {
            try
            {
                var ticket = DataCliente.GetLlamandoCliente();

                if (ticket != null)
                {
                    boxTicket.Text = ticket.CodTicket;
                    boxVentana.Text = ticket.Ventanilla.ToString();

                    string mensaje = $"{boxTicket.Text}  PASE A VENTANILLA  {boxVentana.Text}";
                    _speechSynthesizerObj = new SpeechSynthesizer();
                    _speechSynthesizerObj.SpeakCompleted += Synth_SpeakCompleted;
                    foreach (InstalledVoice voice in _speechSynthesizerObj.GetInstalledVoices())
                    {
                        _vocesInfo.Add(voice.VoiceInfo);
                    }

                    var indice = 0;
                    string nombre = _vocesInfo.ElementAt(indice).Name;
                    if (mensaje != "")
                    {
                        _nombre = nombre;
                        _mensaje = mensaje;
                        _ticket = ticket;
                        string x = (Assembly.GetEntryAssembly().Location + "");
                        x = x.Replace("TerminalTicket.exe", "doorbell_x.wav");
                        SoundPlayer sonidoPlay = new SoundPlayer(x);
                        sonidoPlay.Play();
                        _timerDingDong = new Timer { Interval = 2500 };
                        _timerDingDong.Elapsed += OnTimedEvent_TimerDingDong;
                        _timerDingDong.AutoReset = true;
                        _timerDingDong.Enabled = true;
                    }
                    else
                    {
                        ElementVideo.Volume = 0;
                        boxTicket.Text = "";
                        boxVentana.Text = "";
                    }
                }
                else
                {
                    ElementVideo.Volume = 0;
                    boxTicket.Text = "";
                    boxVentana.Text = "";
                }
                _timerCliente.Start();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void OnTimedEvent_TimerDingDong(object source, ElapsedEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal,
                (Action)(() =>
                {
                    _frmLlamado = new frmLlamado { Owner = this };
                    _frmLlamado.WindowStartupLocation = WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    _frmLlamado.Show();



                    ElementVideo.Volume = 0;
                    _speechSynthesizerObj = new SpeechSynthesizer();
                    _speechSynthesizerObj.SelectVoice(_nombre);
                    _speechSynthesizerObj.Rate = -2;
                    _speechSynthesizerObj.Volume = 100;
                    _speechSynthesizerObj.SpeakCompleted += Synth_SpeakCompleted;
                    _speechSynthesizerObj.SpeakAsync(_mensaje);
                    DataCliente.UpdateLlamadoCliente(_ticket);
                    _timerDingDong.Stop();
                }));
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Synth_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            try
            {
                _frmLlamado?.Close();
                _frmLlamado = null;
                _timerCliente.Start();
                ElementVideo.Volume = 0;
                CargarClientes();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                //TimerLoad.Stop();

                FrmAdmon frmAdmon=new FrmAdmon();
                frmAdmon.Owner = this;
                frmAdmon.ShowDialog();
                //TimerLoad.Start();
            }

            if (e.Key == Key.F2)
            {
                DialogResult dialogResult = MessageBox.Show("¿Desea Salir del Programa?", "SALIR DE TV TURNO EEH", MessageBoxButtons.YesNo);
                if (dialogResult == System.Windows.Forms.DialogResult.Yes )
                {
                    Process.GetCurrentProcess().Kill();
                }
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                int cont = 0;
                foreach (var pantalla in Screen.AllScreens)
                {
                    cont = cont + 1;
                    if (!pantalla.Primary && cont == Screen.AllScreens.Count())
                    {

                        WindowStartupLocation = WindowStartupLocation.Manual;
                        Debug.Assert(SystemInformation.MonitorCount > 1);

                        var workingArea = pantalla;
                        var area = workingArea.WorkingArea;
                        Left = area.Left;
                        Top = area.Top;
                        Width = area.Width;
                        Height = area.Height;
                        WindowStartupLocation = WindowStartupLocation.Manual;
                        WindowState = WindowState.Maximized;
                    }
                }
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //MessageBox.Show("error ");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //MessageBox.Show("error ");

            //Your code to handle the event
            e.Cancel = true;
        }
    }
}
