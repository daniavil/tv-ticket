﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalTicket.DTOs;
using TerminalTicket.Models;

namespace TerminalTicket.Controllers
{
    public static class DataTicket
    {
        private static readonly int IdSucursal = int.Parse(ConfigurationManager.AppSettings["IdSucursal"]);
        private static readonly DbConnection conIncms = Fun.GetConnection("IncmsConn");

        public static bool CrearTicket(string menu)
        {
            ActualizaNumSiguienteCodAtencion(IdSucursal, menu);

            using (var db = new TicketModel())
            {
                TICKET_Ticket ticket = new TICKET_Ticket
                {
                    IdTicket = Guid.NewGuid(),
                    FechaHora = DateTime.Now,
                    Finalizo = false,
                    FLlamado = DateTime.Now,
                    IdSucursal = IdSucursal,
                    EnCola = true
                };
                var qry = (from vas in db.TICKET_VentanillaAtencionSucursal
                            where vas.TICKET_Atencion.BtnMenu.Equals(menu)
                                  && vas.TICKET_Sucursal.IdSucursal.Equals(IdSucursal) && vas.Habilitado
                            select vas).FirstOrDefault();
                string valNum = "";

                if (qry == null)
                {
                    valNum = "001";
                }
                else
                {
                    if (qry.SigNum < 10)
                    {
                        valNum = "00" + qry.SigNum;
                    }
                    else if (qry.SigNum < 100)
                    {
                        valNum = "0" + qry.SigNum;
                    }
                    else if (qry.SigNum > 99)
                    {
                        valNum = qry.SigNum.ToString();
                    }
                }                               

                ticket.Llamando = false;
                ticket.IdAtencion = qry.IdAtencion;
                ticket.Sucursal = qry.TICKET_Sucursal.IdSucursal;
                ticket.CodTicket = qry.TICKET_Atencion.CodTipo + valNum;
                ticket.esPrioridad = DataAtencion.GetAtencionById(qry.IdAtencion).EsTercero;

                db.TICKET_Ticket.Add(ticket);
                if (db.SaveChanges() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static ObjClass GetUltimoTicketBySucursal(int sucursal)
        {
            using (var db = new TicketModel())
            {
                ObjClass Ticket = (from tkt in db.TICKET_Ticket
                                   from s in db.TICKET_Sucursal
                                   where tkt.Sucursal == sucursal && tkt.IdSucursal == s.IdSucursal
                                   orderby tkt.FechaHora descending
                                   select new ObjClass() { Id = tkt.CodTicket.ToString(),
                                                           Texto = s.Nombre,
                                                           Texto1 = tkt.FechaHora.ToString()
                                   }).FirstOrDefault();

                if (Ticket!=null)
                {
                    Ticket.Texto1 = Convert.ToDateTime(Ticket.Texto1).ToString("dd/MM/yyyy hh:mm:ss tt");
                }

                return Ticket;
            }
        }

        public static void ActualizaNumSiguienteCodAtencion(int idsuc,string menu)
        {
            using (var db = new TicketModel())
            {
                var updNum =
                    db.TICKET_VentanillaAtencionSucursal.Where(x => x.IdSucursal == idsuc && x.TICKET_Atencion.BtnMenu.Equals(menu))
                        .ToList();

                updNum.ForEach(u=>u.SigNum=(u.SigNum==null?1:u.SigNum+1));
                db.SaveChanges();
            }
        }

        public static void ReiniciarCodAtencionBySucursal()
        {
            using (var db = new TicketModel())
            {
                var updNum = db.TICKET_VentanillaAtencionSucursal.Where(x => x.IdSucursal == IdSucursal).ToList();
                updNum.ForEach(u =>
                {
                    u.SigNum = null;
                    u.Atendiendo = false;
                });
                db.SaveChanges();
            }
        }

        public static int GetCountClientesAtencionNotToday()
        {
            using (var db = new TicketModel())
            {
                int total = (from tb in db.TICKET_Ticket
                             where tb.IdSucursal == IdSucursal
                             && DbFunctions.TruncateTime(tb.FechaHora) != DbFunctions.TruncateTime(DateTime.Now)
                             select tb).Count();

                return total;
            }
        }

        public static void MigrarClientesAtencionHistorico()
        {
            using (var db = new TicketModel())
            {
                var lista = (from tb in db.TICKET_Ticket
                             where tb.IdSucursal == IdSucursal
                             && DbFunctions.TruncateTime(tb.FechaHora) != DbFunctions.TruncateTime(DateTime.Now)
                             orderby tb.FechaHora ascending
                             select tb).ToList();

                foreach (TICKET_Ticket tkt in lista)
                {
                    TICKET_Ticket_Historico tkt_hist = new TICKET_Ticket_Historico();
                    tkt_hist.Id_Historico = Guid.NewGuid();
                    tkt_hist.IdAtencion = tkt.IdAtencion;
                    tkt_hist.IdSucursal = tkt.IdSucursal;
                    tkt_hist.IdVentanilla = tkt.IdVentanilla;
                    tkt_hist.FechaHora = tkt.FechaHora;
                    tkt_hist.Atendido = tkt.Atendio == true ? true : false;

                    double TEsperaEnSeconds = (tkt.FAtendido - tkt.FechaHora).GetValueOrDefault().TotalMilliseconds;

                    if (tkt.FAtendido != null && tkt.FechaHora != null)
                    {
                        DateTime Fecha = (new DateTime(1990, 1, 1)).AddMilliseconds(TEsperaEnSeconds);
                        tkt_hist.TiempoEspera = Fecha.ToString("HH:mm:ss.fff");
                    }

                    double TAtendidoEnSeconds = (tkt.FechaHoraFin - tkt.FAtendido).GetValueOrDefault().TotalMilliseconds;

                    if (tkt.FechaHoraFin != null && tkt.FAtendido != null)
                    {
                        DateTime Fecha1 = (new DateTime(1990, 1, 1)).AddMilliseconds(TAtendidoEnSeconds);
                        tkt_hist.TiempoAtencion = Fecha1.ToString("HH:mm:ss.fff");
                    }

                    db.TICKET_Ticket_Historico.Add(tkt_hist);
                    db.Entry(tkt).State = EntityState.Deleted;
                    db.SaveChanges();
                }

            }
        }

        public static string RetornarUltimoTicket()
        {
            using (var db = new TicketModel())
            {
                var ticket = (from tb in db.TICKET_Ticket
                    where tb.IdSucursal == IdSucursal
                    && DbFunctions.TruncateTime(tb.FechaHora) == DbFunctions.TruncateTime(DateTime.Now)
                    orderby tb.FechaHora descending
                    select tb.CodTicket).FirstOrDefault();

                return ticket;
            }
        }

        public static RespGenerica VerificarTookenValido(string token)
        {
            RespGenerica respuesta = new RespGenerica();
            try
            {
                respuesta.esValido = false;
                respuesta.msjResp = "TOKEN NO VALIDO.!";

                using (var db = new TicketModel())
                {
                    var tokenObj = db.TICKET_TokenApp
                        .Where(x => x.token.Equals(token)
                        && DbFunctions.TruncateTime(x.fHoraArriboPrevisto) == DbFunctions.TruncateTime(DateTime.Now)
                        && !x.arribo)
                        .FirstOrDefault();

                    if (tokenObj != null)
                    {
                        var fechaHoraMinutoMaxima = tokenObj.fHoraArriboPrevisto.AddMinutes(int.Parse(ConfigurationManager.AppSettings["minMax"]));

                        if (DateTime.Now < fechaHoraMinutoMaxima)
                        {
                            respuesta.esValido = true;
                            tokenObj.vencido = false;
                        }
                        else
                        {
                            respuesta.esValido = false;
                            respuesta.msjResp = "TOKEN VENCIDO POR TIEMPO.!";
                            tokenObj.vencido = true;
                        }
                        tokenObj.fechaHoraArribo = DateTime.Now;
                        tokenObj.arribo = true;

                        db.Entry(tokenObj).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return respuesta;
            }
            catch (Exception)
            {
                respuesta.esValido = false;
                respuesta.msjResp = "TOKEN NO VALIDO.!";
                return respuesta;
            }
        }

        public static RespGenerica VerificarClienteEmpresarial(string clave)
        {
            RespGenerica respuesta = new RespGenerica();
            try
            {
                string sql = $@"select top 10 a.nis_rad AS nis_rad, f.pan_cuenta_2 as clave,f.nombre_cliente nombre
                                ,a.tip_mercado as codMercado,e.desc_tipo as mercado,
                                IIF(a.tip_mercado<>'TY006',0,1) as EsEmp
                                from sumcon a, clientes b, tipos c, tipos e, maestro_suscriptores f
                                where a.cod_cli=b.cod_cli 
                                and a.tip_tension=c.tipo
                                and a.nis_rad=f.nis_rad
                                and a.tip_mercado = e.tipo
                                and f.pan_cuenta_2={clave}";
                try
                {
                    var cliente = SqlMapper.QueryFirst<ClienteDto>(conIncms, sql);

                    if (cliente.EsEmp)
                    {
                        respuesta.msjResp = "Cliente Empresarial válido.";
                    }
                    else
                    {
                        respuesta.msjResp = $"No está catalogado como cliente Empresarial. Por favor seleccionar otra opción.";
                    }
                    respuesta.esValido = cliente.EsEmp;
                }
                catch (Exception ex)
                {
                    respuesta.esValido = false;
                    respuesta.msjResp = "CLAVE NO VALIDA !";
                }
                return respuesta;
            }
            catch (Exception)
            {
                respuesta.esValido = false;
                respuesta.msjResp = "CLAVE NO VALIDA !";
                return respuesta;
            }
        }
    }
}
