﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalTicket.Models;

namespace TerminalTicket.Controllers
{
    public static class DataAtencion
    {
        public static TICKET_Atencion GetAtencionById(int idAtencion)
        {
            using (var db = new TicketModel())
            {
                var atencion = db.TICKET_Atencion.Find(idAtencion);
                return atencion;
            }
        }
    }
}
