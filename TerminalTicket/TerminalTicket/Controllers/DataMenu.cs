﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalTicket.Models;

namespace TerminalTicket.Controllers
{
    public static class DataMenu
    {
        private static readonly int IdSucursal = int.Parse(ConfigurationManager.AppSettings["IdSucursal"]);
        public static List<ObjClass> GetMenuBySucursal()
        {
            using (var db = new TicketModel())
            {
                var qry = (from mnu in db.TICKET_Atencion
                           select new ObjClass
                           {
                               Id = mnu.IdAtencion.ToString(),
                               Texto = mnu.Nombre
                           }).ToList();
                return qry;
            }
        }


        public static bool MuestraOcultaBotonMenu(string nombreBoton)
        {
            using (var db = new TicketModel())
            {
                var qry = (from mnu in db.TICKET_Atencion
                    join vas in db.TICKET_VentanillaAtencionSucursal on mnu.IdAtencion equals vas.IdAtencion
                    where mnu.BtnMenu.Equals(nombreBoton) && vas.IdSucursal==IdSucursal
                           select vas.Habilitado).FirstOrDefault();
                return qry;
            }
        }

    }
}