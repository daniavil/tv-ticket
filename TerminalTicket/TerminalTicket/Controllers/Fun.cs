﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Serialization;
using TerminalTicket.Models;
//using ModuloRCM.DataTransferObject;

namespace TerminalTicket.Controllers
{
    public class Fun
    {
        private static readonly int IdSucursal = int.Parse(ConfigurationManager.AppSettings["IdSucursal"]);
        private static readonly DbConnection conStrSicra = GetConnection("IncmsConn");

        /// <summary>
        /// Funcion para generar una consulta a base de datos enviado como parametro strind la consulta a realizar.
        /// </summary>
        /// <param name="pQuery"></param>
        /// <param name="pCmdTimeOut"></param>
        /// <param name="pConnectionString"></param>
        /// <returns>Retorna un dataset como respuesta</returns>
        public static DataSet GetData(string pQuery, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {
            DataSet ds = new DataSet();
            string conStr = "";
            conStr=ConfigurationManager.ConnectionStrings[pConnectionString].ToString();

            SqlConnection sqlCon=new SqlConnection(conStr);
            sqlCon.Open();
            SqlCommand sqlComm=new SqlCommand(pQuery,sqlCon);

            SqlDataAdapter sqlAdap = new SqlDataAdapter(sqlComm);
            sqlAdap.SelectCommand.CommandTimeout = pCmdTimeOut;
            sqlAdap.Fill(ds);
            sqlCon.Close();
            sqlCon.Dispose();
            return ds;
        }

        public static DataSet GetPaData(string paName,List<ObjClass> parametros, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {
            DataSet ds = new DataSet();
            string conStr = "";
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();

            SqlConnection sqlCon = new SqlConnection(conStr);
            sqlCon.Open();
            SqlCommand sqlComm = new SqlCommand(paName, sqlCon);
            sqlComm.CommandType = CommandType.StoredProcedure;

            foreach (ObjClass objeto in parametros)
            {
                sqlComm.Parameters.Add(new SqlParameter("@" + objeto.Texto, objeto.Texto1));
            }
            
            SqlDataAdapter sqlAdap = new SqlDataAdapter(sqlComm);
            sqlAdap.SelectCommand.CommandTimeout = pCmdTimeOut;
            sqlAdap.Fill(ds);
            sqlCon.Close();
            sqlCon.Dispose();
            return ds;
        }

        public static string ExecPaQuery(string paName, List<ObjClass> parametros, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {
            string conStr = "";
            int result = 0;
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();
            string estado = "";

            using (SqlConnection con = new SqlConnection(conStr))
            {
                using (SqlCommand cmd = new SqlCommand(paName, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (ObjClass objeto in parametros)
                    {
                        cmd.Parameters.Add(new SqlParameter("@" + objeto.Texto, objeto.Texto1));
                    }

                    con.Open();
                    result = cmd.ExecuteNonQuery();
                    estado = result > 0 ? "ok" : "error";
                }
            }
            return estado;
        }

        public static List<ObjClass> ExecPaQueryResult(string paName, List<SqlParameter> parametros, List<SqlParameter> parametrosOutput, int pCmdTimeOut = 30, string pConnectionString = "SegConn")
        {
            string conStr = "";
            int result = 0;
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();
            string estado = "";
            List<ObjClass> lst = new List<ObjClass>();

            using (SqlConnection con = new SqlConnection(conStr))
            {
                using (SqlCommand cmd = new SqlCommand(paName, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    foreach (SqlParameter paramIn in parametros)
                    {
                        cmd.Parameters.Add(paramIn);
                    }
                    foreach (SqlParameter paramOut in parametrosOutput)
                    {
                        cmd.Parameters.Add(paramOut).Direction = ParameterDirection.Output;                        
                    }
                   
                    con.Open();
                    result = cmd.ExecuteNonQuery();
                    estado = result > 0 ? "ok" : "error";

                    
                    foreach (SqlParameter paramOut in parametrosOutput)
                    {
                        lst.Add(new ObjClass() { Texto = paramOut.ParameterName, Texto1 = Convert.ToString(cmd.Parameters[paramOut.ParameterName].Value.ToString()) });
                    }
                  
                }
                                
            }

            return lst;

        }

        public static string ExecQuery(string pQuery, int pCmdTimeOut = 30, string pConnectionString = "SAConn")
        {

            int result = 0;
            string estado = "";
            string conStr = "";
            conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ToString();
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                using (SqlCommand command = new SqlCommand(pQuery, connection))
                {
                    connection.Open();
                    result = command.ExecuteNonQuery();
                    estado = result > 0 ? "ok" : "error";
                }
            }
            return estado;
        }

        public static string CodeKeyStringEncode(string value, string key)
        {
            string ReturnValue = null;
            System.Security.Cryptography.MACTripleDES mac3des = new System.Security.Cryptography.MACTripleDES();
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            mac3des.Key = md5.ComputeHash(System.Text.Encoding.UTF32.GetBytes(key));

            ReturnValue = Convert.ToBase64String(System.Text.Encoding.UTF32.GetBytes(value)) + '-' + Convert.ToBase64String(mac3des.ComputeHash(System.Text.Encoding.UTF32.GetBytes(value)));

            return ReturnValue;
        }

        public static string CodeKeyStringDecode(string value, string key)
        {
            string dataValue = "";
            string calcHash = "";
            string storedHash = "";

            System.Security.Cryptography.MACTripleDES mac3des = new System.Security.Cryptography.MACTripleDES();
            System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            mac3des.Key = md5.ComputeHash(System.Text.Encoding.UTF32.GetBytes(key));

            try
            {
                dataValue = System.Text.Encoding.UTF32.GetString(Convert.FromBase64String(value.Split('-')[0]));
                storedHash = System.Text.Encoding.UTF32.GetString(Convert.FromBase64String(value.Split('-')[1]));
                calcHash = System.Text.Encoding.UTF32.GetString(mac3des.ComputeHash(System.Text.Encoding.UTF32.GetBytes(dataValue)));

                if (storedHash != calcHash)
                {
                    throw new ArgumentException("Información Invalida");
                }
            }
            catch (Exception ex)
            {
                //Throw New ArgumentException("Llave Invalida")
                return "Llave Invalida";
            }

            return dataValue;

        }

        public static string encrypt(string plainText, string Token)
        {
            string key = "";
            string iv = "";
            key = Token.Substring(0, 108);
            iv = Token.Substring(108, 64);
            byte[] _key = Convert.FromBase64String(key);
            byte[] _iv = Convert.FromBase64String(iv);
            int keySize = 32;
            int ivSize = 16;
            Array.Resize(ref _key, keySize);
            Array.Resize(ref _iv, ivSize);
            Rijndael RijndaelAlg = new RijndaelManaged();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, RijndaelAlg.CreateEncryptor(_key, _iv), CryptoStreamMode.Write);
            byte[] plainMessageBytes = UTF8Encoding.UTF8.GetBytes(plainText);
            cryptoStream.Write(plainMessageBytes, 0, plainMessageBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherMessageBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(cipherMessageBytes);
        }

        public static string decrypt(string plainText, string Token)
        {
            string key = "";
            string iv = "";
            key = Token.Substring(0, 108);
            iv = Token.Substring(108, 64);
            byte[] _key = Convert.FromBase64String(key);
            byte[] _iv = Convert.FromBase64String(iv);
            int keySize = 32;
            int ivSize = 16;
            Array.Resize(ref _key, keySize);
            Array.Resize(ref _iv, ivSize);
            byte[] cipherTextBytes = Convert.FromBase64String(plainText);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];
            Rijndael RijndaelAlg = new RijndaelManaged();
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, RijndaelAlg.CreateDecryptor(_key, _iv), CryptoStreamMode.Read);
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        }

        public static void InsertSQLBulkCopy(DataTable csvFileData, string tablaDestino, string pConnectionString = "FactConn")
        {
            string conStr = "";

            if (string.IsNullOrEmpty(pConnectionString))
            {
                conStr = ConfigurationManager.ConnectionStrings["SICRAModel"].ConnectionString;
            }
            else
            {
                conStr = ConfigurationManager.ConnectionStrings[pConnectionString].ConnectionString;
            }

            using (SqlConnection dbConnection = new SqlConnection(conStr))
            {
                dbConnection.Open();
                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                {
                    s.DestinationTableName = tablaDestino;

                    foreach (var column in csvFileData.Columns)
                        s.ColumnMappings.Add(column.ToString(), column.ToString());

                    s.WriteToServer(csvFileData);
                }
            }
        }

        public static DbConnection GetConnection(string _conn)
        {
            string connectionString = ConfigurationManager.ConnectionStrings[_conn].ConnectionString;
            var factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
            var conn = factory.CreateConnection();
            conn.ConnectionString = connectionString;
            conn.Open();
            return conn;
        }

        public static string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch
            {
                throw;
            }
        }

        public static T Deserialize<T>(string xmlText)
        {
            try
            {
                var stringReader = new System.IO.StringReader(xmlText);
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
            catch
            {
                throw;
            }
        }

        public static int GetCicloActual()
        {
            string sql = "select CONCAT(FORMAT(GETDATE(),'yy'),FORMAT(GETDATE(),'MM'))";
            var cicloFact = SqlMapper.Query<int>(conStrSicra, sql).FirstOrDefault();
            return cicloFact;
        }

        public static void ReportError(string msj,string form)
        {
            var error = new TICKET_Logs
            {
                FechaHora = DateTime.Now,
                Form = form,
                MsjException = msj,
                IdSucursal = IdSucursal
            };

            using (var db = new TicketModel())
            {
                db.Entry(error).State = EntityState.Added;
                db.SaveChanges();
            }

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal,
                (Action)(() =>
                {
                    System.Windows.Forms.Application.Restart();
                    Process.GetCurrentProcess().Kill();
                }));

        }
    }
}