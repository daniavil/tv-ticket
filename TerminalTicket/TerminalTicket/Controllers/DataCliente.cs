﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalTicket.Models;

namespace TerminalTicket.Controllers
{
    public static class DataCliente
    {
        private static readonly int NumeroSucursal = int.Parse(ConfigurationManager.AppSettings["IdSucursal"]);

        public static List<ObjClass> GetListaClientesDto()
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_Ticket
                           where !tb.Finalizo && !tb.EnCola && tb.Sucursal== NumeroSucursal
                           orderby tb.FechaHora ascending
                           select new ObjClass
                           {
                               Id = tb.Ventanilla.ToString(),
                               Texto = tb.CodTicket
                           }).ToList();
                return qry;
            }
        }

        public static List<TICKET_Ticket> GetListaClientesEnCola()
        {
            TicketModel db=new TicketModel();
            var qry = db.TICKET_Ticket.Where(t => t.EnCola && !t.Finalizo && t.Sucursal==NumeroSucursal)
                .OrderBy(t => t.FechaHora).ToList();
            return qry;
        }

        public static List<TICKET_Ticket> GetListaClientesEnColaNoLlamado()
        {
            TicketModel db = new TicketModel();
            var qry = db.TICKET_Ticket.Where(t => t.EnCola && !t.Finalizo && !t.Llamando
            && t.IdVentanilla>0 && t.Ventanilla>0 && t.Sucursal == NumeroSucursal)
                .OrderBy(t => t.FechaHora).ToList();
            return qry;
        }

        public static TICKET_Ticket GetSiguienteClientesEnColaLlamar()
        {
            TicketModel db = new TicketModel();
            var ticketSig = db.TICKET_Ticket.Where(t => t.EnCola && !t.Finalizo
            && !t.Llamando && t.IdVentanilla>0 && t.Ventanilla>0 && t.Sucursal == NumeroSucursal)
                .OrderBy(t => t.FLlamado).FirstOrDefault();

            var ventanilla =
                db.TICKET_VentanillaAtencionSucursal
                    .Count(v => v.TICKET_Ventanilla.NumVentanilla==ticketSig.Ventanilla && v.Atendiendo);

            if (ventanilla>0)
            {
                return ticketSig;
            }
            else
            {
                return null;
            }
        }

        public static TICKET_Ticket GetLlamandoCliente()
        {
            TicketModel db = new TicketModel();
            var qry = db.TICKET_Ticket.FirstOrDefault(t => t.EnCola && !t.Finalizo
            && t.Llamando && t.IdVentanilla>0 && t.Ventanilla>0 && t.Sucursal == NumeroSucursal);
            return qry;
        }

        public static void UpdateLlamadoCliente(TICKET_Ticket ticket)
        {
            if (GetListaClientesEnColaNoLlamado().Count > 0)
            {

                using (var db = new TicketModel())
                {
                    var updTicket = db.TICKET_Ticket.Find(ticket.IdTicket);
                    updTicket.Llamando = false;
                    db.TICKET_Ticket.Attach(updTicket);
                    db.Entry(updTicket).State = EntityState.Modified;

                    TICKET_Ticket siguienteTicket = new TICKET_Ticket();
                    siguienteTicket = GetSiguienteClientesEnColaLlamar();

                    if (siguienteTicket != null)
                    {
                        var updSigTicket = new TICKET_Ticket();
                        updSigTicket = db.TICKET_Ticket.Find(siguienteTicket.IdTicket);
                        updSigTicket.Llamando = true;
                        updSigTicket.FLlamado = DateTime.Now;
                        db.TICKET_Ticket.Attach(updSigTicket);
                        db.Entry(updSigTicket).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                }
            }
        }

    }
}
