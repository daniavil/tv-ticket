﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalTicket.Models;

namespace TerminalTicket.Controllers
{
    public static class DataSucursal
    {
        private static readonly int IdSucursal = int.Parse(ConfigurationManager.AppSettings["IdSucursal"]);
        public static string GetNombreSucursal()
        {
            using (var db = new TicketModel())
            {
                var qry = (from scs in db.TICKET_Sucursal
                    where scs.IdSucursal == IdSucursal
                    select scs.Nombre).FirstOrDefault();
                return qry;
            }
        }
    }
}
