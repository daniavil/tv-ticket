﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using TerminalTicket.Models;

namespace TerminalTicket.Controllers
{
    public static class DataMensaje
    {
        private static readonly int IdSucursal = int.Parse(ConfigurationManager.AppSettings["IdSucursal"]);

        public static int CountMensajes()
        {
            using (var db = new TicketModel())
            {
                var qry = (from msjs in db.TICKET_Mensaje
                           where msjs.Habilitado && msjs.TICKET_Sucursal.IdSucursal.Equals(IdSucursal)
                           orderby msjs.idMsj descending
                           select msjs).Count();
                return qry;
            }
        }

        public static string MensajebyIndex(int index)
        {
            try
            {
                string msj = "";
                using (var db = new TicketModel())
                {
                    var qry = (from msjs in db.TICKET_Mensaje
                               where msjs.Habilitado && msjs.TICKET_Sucursal.IdSucursal.Equals(IdSucursal)
                               orderby msjs.idMsj ascending
                               select msjs.Mensaje).ToList();
                    msj = qry[index];
                    return msj;
                }
            }
            catch (Exception)
            {
                return null;
            }            
        }
    }
}
