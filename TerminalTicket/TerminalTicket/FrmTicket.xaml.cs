﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using TerminalTicket.Controllers;
using Timer = System.Timers.Timer;

namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para FrmTicket.xaml
    /// </summary>
    public partial class FrmTicket : Window
    {
        public static Timer TimerLoad;
        public FrmTicket()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                lblTicket.Content = DataTicket.RetornarUltimoTicket();
                TimerLoad = new Timer { Interval = 5000 };
                TimerLoad.Elapsed += OnTimedEvent_TimerLoad;
                TimerLoad.AutoReset = true;
                TimerLoad.Enabled = true;
                TimerLoad.Start();


                foreach (var pantalla in Screen.AllScreens)
                {
                    if (pantalla.Primary)
                    {
                        var workingArea = pantalla;
                        var area = workingArea.WorkingArea;
                        this.Left = area.Left;
                        this.Top = area.Top;
                        this.Width = area.Width;
                        this.Height = area.Height;
                        //this.WindowStartupLocation = WindowStartupLocation.Manual;
                        this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        //this.WindowState = WindowState.Maximized;
                        this.Topmost = true;
                        //Width = "650" Height = "350"
                    }
                }
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void OnTimedEvent_TimerLoad(object source, ElapsedEventArgs e)
        {
            try
            {
                TimerLoad.Enabled = false;
                TimerLoad.Stop();
                Dispatcher.Invoke(DispatcherPriority.Normal,
                    (Action)(Close));
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                this.Top = (SystemParameters.WorkArea.Height - this.Height) / 2;
                this.Left = (SystemParameters.WorkArea.Width - this.Width) / 2;
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
