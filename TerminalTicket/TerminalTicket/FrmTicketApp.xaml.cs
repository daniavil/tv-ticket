﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using TerminalTicket.Controllers;
using Cursors = System.Windows.Input.Cursors;

namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para FrmAdmon.xaml
    /// </summary>
    public partial class FrmTicketApp : Window
    {
        public FrmTicketApp()
        {
            InitializeComponent();
            Mouse.OverrideCursor = Cursors.Hand;

            //System.Diagnostics.Process.Start("osk.exe");
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.None;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var pantalla in Screen.AllScreens)
                {
                    if (pantalla.Primary)
                    {
                        var workingArea = pantalla;
                        var area = workingArea.WorkingArea;
                        this.Left = area.Left;
                        this.Top = area.Top;
                        this.Width = area.Width;
                        this.Height = area.Height;
                        //this.WindowStartupLocation = WindowStartupLocation.Manual;
                        this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        //this.WindowState = WindowState.Maximized;
                        this.Topmost = true;
                        this.MaxWidth = 800;
                        this.MaxHeight = 500;
                        //Width = "650" Height = "350"
                    }
                }
                txtCodToken.Focus();
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BtnCancel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void BtnCancel_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            this.Close();
        }

        private void BtnAceptar_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void BtnAceptar_PreviewTouchDown(object sender, TouchEventArgs e)
        {

        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                this.Top = (SystemParameters.WorkArea.Height - this.Height) / 2;
                this.Left = (SystemParameters.WorkArea.Width - this.Width) / 2;
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}
