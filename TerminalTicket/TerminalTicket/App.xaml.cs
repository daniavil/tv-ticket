﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using TerminalTicket.Controllers;
using Application = System.Windows.Application;

namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        public static void Main()
        {
            try
            {
                int id = Process.GetCurrentProcess().SessionId;
                if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
                {
                    foreach (var i in Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName))
                    {
                        if (i.SessionId != id)
                        {
                            i.Kill();
                        }
                    }
                    Iniciar();
                    //System.Windows.MessageBox.Show("Esta aplicación ya esta en ejecución, cierre las demás instancias abiertas.");
                }
                else
                {
                    Iniciar();
                }
            }
            catch (Exception ex)
            {
                Fun.ReportError(ex.Message + ex.StackTrace,
                    MethodBase.GetCurrentMethod().DeclaringType.Name + "/" +
                    MethodBase.GetCurrentMethod().Name);
            }
        }
        protected override void OnExit(ExitEventArgs e)
        {
            //base.OnExit(e);
            //System.Windows.Forms.Application.Restart();
        }

        private static void Iniciar()
        {
            if (ControlVersion())
            {
                var application = new App();
                application.InitializeComponent();
                application.Run();
            }
            else
            {
                if (System.Windows.Forms.MessageBox.Show("ATENCIÓN.! EXISTE UNA VERSIÓN MAS RECIENTE DE ESTE SISTEMA. POR FAVOR CONTACTAR A TIC PARA ACTUALIZAR ESTE SISTEMA",
                    "INFORMACIÓN", MessageBoxButtons.OK) == DialogResult.OK)
                {
                    Process.GetCurrentProcess().Kill();
                }
            }
        }

        static bool ControlVersion()
        {
            string IdSys = ConfigurationManager.AppSettings["IdSys"];
            string AppName = Assembly.GetEntryAssembly().GetName().Name;
            try
            {
                bool abrir = false;
                string conn = ConfigurationManager.ConnectionStrings["SysConn"].ConnectionString;
                using (SqlConnection sqlConnection = new SqlConnection(conn))
                {
                    string qry = $@"select case COUNT(*) when 0 then 'False' else 'True' end AS VERCORRECTA, IdVersion
                    from SysControlVersion
                    where IdSistema='{IdSys}' and NomEnsamblado='{AppName}'
                    AND IdVersion=(select MAX(IdVersion) from SysControlVersion where NomEnsamblado='{AppName}')
                    group by IdVersion";
                    SqlCommand sqlCmd = new SqlCommand(qry, sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        abrir = Convert.ToBoolean(sqlReader["VERCORRECTA"].ToString());
                        ConfigClass.idVersion = sqlReader["IdVersion"].ToString();
                    }
                    sqlReader.Close();
                }
                return abrir;
            }
            catch (Exception EX)
            {
                System.Windows.MessageBox.Show($"SE PRODUJO UNA EXCEPCIÓN: {EX.Message}");
                return false;
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {

        }
    }
}
