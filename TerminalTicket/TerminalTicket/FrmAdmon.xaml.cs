﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace TerminalTicket
{
    /// <summary>
    /// Lógica de interacción para FrmAdmon.xaml
    /// </summary>
    public partial class FrmAdmon : Window
    {
        private string uriDesde="";
        private string uriHasta="";
        private string nuevoNombreVideo="";

        public FrmAdmon()
        {
            InitializeComponent();
            Mouse.OverrideCursor = Cursors.Hand;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.None;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Video files|*.avi;*.mp4;*.wma;*.wmv";
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            if (openFileDialog.ShowDialog() == true)
            {
                uriDesde = openFileDialog.FileName.Trim();
                nuevoNombreVideo = openFileDialog.SafeFileNames[0].Trim();
                uriHasta = (Assembly.GetEntryAssembly().Location);
                uriHasta = uriHasta.Replace("TerminalTicket.exe", nuevoNombreVideo);
                txtVideo.Text = nuevoNombreVideo;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                //modificar solo sucursal
                if (uriDesde == "" || uriHasta == "")
                {
                    //*****Modificar Numero de Sucursal
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["IdSucursal"].Value = txtSede.Text.Trim();
                    config.Save(ConfigurationSaveMode.Modified);

                    //*****Reiniciar Aplicacion
                    System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                    Application.Current.Shutdown();
                }
                else
                {
                    //*****Copiar Nuevo Video
                    File.Copy(uriDesde, uriHasta);
                    if (CheckFileHasCopied(uriHasta))
                    {
                        //*****Modificar Numero de Sucursal
                        Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        config.AppSettings.Settings["IdSucursal"].Value = txtSede.Text.Trim();
                        config.Save(ConfigurationSaveMode.Modified);
                        //*****Modificar Nombre de video
                        config.AppSettings.Settings["NombreVideo"].Value = nuevoNombreVideo;
                        config.Save(ConfigurationSaveMode.Modified);

                        //*****Reiniciar Aplicacion
                        System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
                        Application.Current.Shutdown();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: "+ex);
            }
        }

        private bool CheckFileHasCopied(string FilePath)
        {
            try
            {
                if (File.Exists(FilePath))
                    using (File.OpenRead(FilePath))
                    {
                        return true;
                    }
                else
                    return false;
            }
            catch (Exception)
            {
                //Thread.Sleep(100);
                return CheckFileHasCopied(FilePath);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string NumeroSucursal = ConfigurationManager.AppSettings["IdSucursal"];
            string NombreVideo = ConfigurationManager.AppSettings["NombreVideo"];
            txtSede.Text = NumeroSucursal.ToString();
            txtVideo.Text = NombreVideo.ToString();
        }
    }
}
