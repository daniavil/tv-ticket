﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TerminalTicket
{
    public partial class MessageBoxError : Form
    {
        public MessageBoxError(string msjError)
        {
            InitializeComponent();
            try
            {
                txtMsj.Text = msjError;
            }
            catch (Exception ex)
            {
                txtMsj.Text = ex.Message;
            }
        }

        private void MessageBoxError_Load(object sender, EventArgs e)
        {
            Cursor.Hide();
            btnCerrar.Focus();
            txtMsj.SelectionStart = txtMsj.Text.Length;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
