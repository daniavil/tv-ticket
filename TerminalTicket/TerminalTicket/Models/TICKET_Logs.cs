namespace TerminalTicket.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_Logs
    {
        [Key]
        public int IdEvento { get; set; }

        [Required]
        public string Form { get; set; }

        [Required]
        public string MsjException { get; set; }

        public DateTime FechaHora { get; set; }

        public int IdSucursal { get; set; }
    }
}
