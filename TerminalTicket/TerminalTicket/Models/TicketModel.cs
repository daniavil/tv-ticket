using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace TerminalTicket.Models
{
    public partial class TicketModel : DbContext
    {
        public TicketModel()
            : base("name=TicketConn")
        {
        }

        public virtual DbSet<TICKET_Atencion> TICKET_Atencion { get; set; }
        public virtual DbSet<TICKET_Logs> TICKET_Logs { get; set; }
        public virtual DbSet<TICKET_Mensaje> TICKET_Mensaje { get; set; }
        public virtual DbSet<TICKET_Sucursal> TICKET_Sucursal { get; set; }
        public virtual DbSet<TICKET_Ticket> TICKET_Ticket { get; set; }
        public virtual DbSet<TICKET_Ticket_Historico> TICKET_Ticket_Historico { get; set; }
        public virtual DbSet<TICKET_TokenApp> TICKET_TokenApp { get; set; }
        public virtual DbSet<TICKET_Ventanilla> TICKET_Ventanilla { get; set; }
        public virtual DbSet<TICKET_VentanillaAtencionSucursal> TICKET_VentanillaAtencionSucursal { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TICKET_Atencion>()
                .Property(e => e.CodTipo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TICKET_Atencion>()
                .Property(e => e.BtnMenu)
                .IsFixedLength();

            modelBuilder.Entity<TICKET_Atencion>()
                .HasMany(e => e.TICKET_Atencion1)
                .WithOptional(e => e.TICKET_Atencion2)
                .HasForeignKey(e => e.SegAtencion);

            modelBuilder.Entity<TICKET_Atencion>()
                .HasMany(e => e.TICKET_VentanillaAtencionSucursal)
                .WithRequired(e => e.TICKET_Atencion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TICKET_Sucursal>()
                .HasMany(e => e.TICKET_VentanillaAtencionSucursal)
                .WithRequired(e => e.TICKET_Sucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TICKET_Ventanilla>()
                .HasMany(e => e.TICKET_VentanillaAtencionSucursal)
                .WithRequired(e => e.TICKET_Ventanilla)
                .WillCascadeOnDelete(false);
        }
    }
}
